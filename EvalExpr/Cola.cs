﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvalExpr
{
    class Cola<T>
    {
        class Nodo
        {
            public T info;
            public Nodo sig;
        }

        private Nodo raiz, fondo;

        public Cola()
        {
            raiz = null;
            fondo = null;
        }

        public void push(T x)
        {
            Nodo nuevo;
            nuevo = new Nodo();
            nuevo.info = x;
            nuevo.sig = null;
            if (empty())
                raiz = nuevo;
            else
                fondo.sig = nuevo;
            fondo = nuevo;
        }

        public T pop()
        {
            T info = raiz.info;
            if (raiz == fondo)
            {
                raiz = null;
                fondo = null;
            }
            else
            {
                raiz = raiz.sig;
            }
            return info;
        }

        public bool empty()
        {
            return raiz == null;
        }

    }
}
