﻿using System;
using System.Windows.Forms;

namespace EvalExpr
{
    public partial class frmEvalExpr : Form
    {
        // Expresión actual
        private Expression _expr = null;
        private String _posfija = "";

        public frmEvalExpr()
        {
            InitializeComponent();
        }

        public static bool esOperador(char c)
        {
            return c == '^' || c == '*' || c == '/' || c == '+' || c == '-';
        }

        /*
         Precedencia (Jerarquía de los Operadores)
            Operador    Posfijo    Pila
               ^           4         3
               *           2         2
               /           2         2
               +           1         1
               -           1         1
               (           5         0
        */

        // Devuelve la Prioridad en la Expresión
        public static int pe(char operador)
        {
            if (operador == '^') return 4;
            if (operador == '*' || operador == '/') return 2;
            if (operador == '+' || operador == '-') return 1;
            if (operador == '(') return 5;
            return 0;
        }

        // Devuelve la Prioridad en la Pila
        public static int pp(char operador)
        {
            if (operador == '^') return 3;
            if (operador == '*' || operador == '/') return 2;
            if (operador == '+' || operador == '-') return 1;
            if (operador == '(') return 0;
            return 0;
        }

        public static string convertirAPosfija(String infija)
        {
            String _postfija = "|";
            Pila<char> operators = new Pila<char>();

            for(int i=0; i < infija.Length; i++)
            {
                char car = infija[i];

                if (car == '(')
                {
                    operators.push(car);
                }
                else
                {
                    if (car == ')')
                    {
                        while (operators.top() != '(')
                        {
                            _postfija += "|";
                            _postfija += operators.pop();
                        }
                            
                        car = operators.pop();
                    }
                    else
                    {
                        if (esOperador(car))
                        {
                            if (operators.empty())
                            {
                                operators.push(car);
                                _postfija += "|";
                            }
                            else
                                if (pe(car) > pp(operators.top()))
                                {
                                    operators.push(car);
                                    _postfija += "|";
                                }    
                                else
                                {
                                    _postfija += "|";
                                    _postfija += operators.pop();
                                    operators.push(car);
                                    _postfija += "|";
                                }
                        }
                        else
                        {
                            _postfija += car;
                        }        
                    }
                }
            }

            while (!operators.empty())
            {
                _postfija += "|";
                _postfija += operators.pop();
            }

            _postfija += "|";
            return _postfija;
        }

        public static double evaluarExpresion(String posfija)
        {

            return 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                // Clear the old constants and variables
                // Borrar las constantes y variables de la expresión anterior
                ExpressionBase.Clear();

                _expr = new Expression();
                
                // Analizar y compilar la expresión en un objeto Expression
                string sexpr = _expr.Parse(txtExpression.Text);

                if (!string.IsNullOrEmpty(sexpr.Trim(new char[] { ' ', '\n', '\r', '\t' })))
                {
                    throw new BadSyntaxException(sexpr);
                }

                _posfija = convertirAPosfija(txtExpression.Text);
                MessageBox.Show(_posfija);
                
            }
            catch (BadSyntaxException)
            {
                MessageBox.Show("Error de Sintaxis","Error");
            }
            catch (NotRecognizedException)
            {

                MessageBox.Show("Cadena Vacía", "Error");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }
    }
}
