﻿using System;
using System.Collections.Generic;

namespace EvalExpr
{
    public abstract class ExpressionBase
    {
        protected static int _precision = 10;
        protected static List<Variable> _variables = new List<Variable>();
        protected static List<Constant> _constants = new List<Constant>();

        public ExpressionBase() { }
        public static int Precision
        {
            get { return ExpressionBase._precision; }
            set { ExpressionBase._precision = Math.Min(20, Math.Max(1, value)); }
        }
        public IEnumerable<Variable> Variables
        {
            get
            {
                foreach (Variable v in ExpressionBase._variables)
                { yield return v; }
            }
        }
        public IEnumerable<Constant> Constants
        {
            get
            {
                foreach (Constant c in ExpressionBase._constants)
                { yield return c; }
            }
        }
        public static void Clear()
        {
            _variables.Clear();
            _constants.Clear();
        }
        public abstract double Value { get; set; }
        public virtual string Parse(string expr)
        {
            return expr.TrimStart(new char[] { ' ', '\n', '\r', '\t' });
        }
    }
}
