﻿using System;

namespace EvalExpr
{
    public class NotRecognizedException : Exception
    {
    }
    public class BadSyntaxException : Exception
    {
        public BadSyntaxException(string text)
            : base(text)
        {
        }
    }
}
