﻿using System;
using System.Linq;

namespace EvalExpr
{
    public class Variable : ExpressionBase, IComparable<Variable>, IEquatable<Variable>
    {
        private double _value = double.NaN;
        private string _name = null;

        public Variable()
            : base()
        {
        }
        public override double Value
        {
            get
            {
                return _value;
            }
            set
            {
                _value = value;
            }
        }
        public override string Parse(string expr)
        {
            string svar = base.Parse(expr);
            if (!string.IsNullOrEmpty(svar))
            {
                if ((char.ToLower(svar[0]) >= 'x') && (char.ToLower(svar[0]) <= 'z'))
                {
                    _name = svar.Substring(0, 1).ToLower();
                    if (!ExpressionBase._variables.Contains<Variable>(this))
                    {
                        ExpressionBase._variables.Add(this);
                    }
                    if (svar.Length > 1)
                    {
                        return svar.Substring(1);
                    }
                    return "";
                }
            }
            throw new NotRecognizedException();
        }
        public int CompareTo(Variable other)
        {
            return string.Compare(_name, other._name, true);
        }
        public bool Equals(Variable other)
        {
            return string.Compare(_name, other._name, true) == 0;
        }
        public override string ToString()
        {
            return _name;
        }
    }
}
