﻿using Expressions;
using System;

namespace EvalExpr
{
    public class Expression : ExpressionBase
    {
        protected ExpressionBase _operand1 = null;
        protected ExpressionBase _operand2 = null;
        protected char _operator = '\0';

        public Expression()
            : base()
        {
        }
        protected Expression(ExpressionBase op1, ExpressionBase op2, char op)
            : base()
        {
            _operand1 = op1;
            _operand2 = op2;
            _operator = op;
        }
        public override double Value
        {
            get
            {
                if (_operand2 == null)
                {
                    if (_operator == '-')
                    {
                        return -_operand1.Value;
                    }
                    return _operand1.Value;
                }
                switch (_operator)
                {
                    case '+':
                        return Math.Round(_operand1.Value + _operand2.Value, ExpressionBase._precision);
                    case '-':
                        return Math.Round(_operand1.Value - _operand2.Value, ExpressionBase._precision);
                    case '*':
                        return Math.Round(_operand1.Value * _operand2.Value, ExpressionBase._precision);
                    case '/':
                        return Math.Round(_operand1.Value / _operand2.Value, ExpressionBase._precision);
                    case '^':
                        return Math.Round(Math.Pow(_operand1.Value, _operand2.Value), ExpressionBase._precision);
                    default:
                        return double.NaN;
                }
            }
            set
            {
                throw new NotSupportedException();
            }
        }
        public override string Parse(string expr)
        {
            string sexpr = base.Parse(expr);
            if (!string.IsNullOrEmpty(sexpr))
            {
                _operand1 = Expression2(ref sexpr);
                sexpr = base.Parse(sexpr);
                while (!string.IsNullOrEmpty(sexpr))
                {
                    if (sexpr[0] == ')')
                    {
                        break;
                    }
                    else if ((sexpr[0] == '+') || (sexpr[0] == '-'))
                    {
                        if (_operand2 != null)
                        {
                            _operand1 = new Expression(_operand1, _operand2, _operator);
                        }
                        _operator = sexpr[0];
                        if (sexpr.Length > 1)
                        {
                            sexpr = base.Parse(sexpr.Substring(1));
                            _operand2 = Expression2(ref sexpr);
                            sexpr = base.Parse(sexpr);
                        }
                        else
                        {
                            throw new BadSyntaxException(sexpr);
                        }
                    }
                    else
                    {
                        throw new BadSyntaxException(sexpr);
                    }
                }
                if ((_operand2 == null) && (_operator != '-'))
                {
                    Expression exp = _operand1 as Expression;
                    if (exp != null)
                    {
                        _operand1 = exp._operand1;
                        _operand2 = exp._operand2;
                        _operator = exp._operator;
                    }
                }
                return sexpr;
            }
            throw new NotRecognizedException();
        }
        private ExpressionBase Expression2(ref string expr)
        {
            ExpressionBase operand1 = Expression1(ref expr);
            ExpressionBase operand2 = null;
            char op = '\0';
            expr = base.Parse(expr);
            while (!string.IsNullOrEmpty(expr))
            {
                if ((expr[0] == '*') || (expr[0] == '/'))
                {
                    if (operand2 != null)
                    {
                        operand1 = new Expression(operand1, operand2, op);
                    }
                    op = expr[0];
                    if (expr.Length > 1)
                    {
                        expr = base.Parse(expr.Substring(1));
                        operand2 = Expression2(ref expr);
                        expr = base.Parse(expr);
                    }
                    else
                    {
                        throw new BadSyntaxException(expr);
                    }
                }
                else
                {
                    break;
                }
            }
            if (operand2 != null)
            {
                return new Expression(operand1, operand2, op);
            }
            return operand1;
        }
        private ExpressionBase Expression1(ref string expr)
        {
            ExpressionBase operand1 = Expression0(ref expr);
            ExpressionBase operand2 = null;
            char op = '\0';
            expr = base.Parse(expr);
            while (!string.IsNullOrEmpty(expr))
            {
                if (expr[0] == '^')
                {
                    if (operand2 != null)
                    {
                        operand1 = new Expression(operand1, operand2, op);
                    }
                    op = expr[0];
                    if (expr.Length > 1)
                    {
                        expr = base.Parse(expr.Substring(1));
                        operand2 = Expression0(ref expr);
                        expr = base.Parse(expr);
                    }
                    else
                    {
                        throw new BadSyntaxException(expr);
                    }
                }
                else
                {
                    break;
                }
            }
            if (operand2 != null)
            {
                return new Expression(operand1, operand2, op);
            }
            return operand1;
        }
        private ExpressionBase Expression0(ref string expr)
        {
            if (!string.IsNullOrEmpty(expr))
            {
                if (expr[0] == '-')
                {
                    char op = expr[0];
                    if (expr.Length > 1)
                    {
                        expr = base.Parse(expr.Substring(1));
                        ExpressionBase em = Element(ref expr);
                        expr = base.Parse(expr);
                        return new Expression(em, null, op);
                    }
                }
                else
                {
                    return Element(ref expr);
                }
            }
            throw new BadSyntaxException(expr);
        }
        private ExpressionBase Element(ref string expr)
        {
            if (!string.IsNullOrEmpty(expr))
            {
                try
                {
                    Number exnum = new Number();
                    expr = exnum.Parse(expr);
                    return exnum;
                }
                catch (NotRecognizedException)
                {
                }
                try
                {
                    Variable exvar = new Variable();
                    expr = exvar.Parse(expr);
                    return ExpressionBase._variables[ExpressionBase._variables.IndexOf(exvar)];
                }
                catch (NotRecognizedException)
                {
                }
                try
                {
                    Constant excon = new Constant();
                    expr = excon.Parse(expr);
                    return ExpressionBase._constants[ExpressionBase._constants.IndexOf(excon)];
                }
                catch (NotRecognizedException)
                {
                }
                if (expr[0] == '(')
                {
                    if (expr.Length > 1)
                    {
                        expr = base.Parse(expr.Substring(1));
                        ExpressionBase expar = new Expression();
                        expr = expar.Parse(expr);
                        expr = base.Parse(expr);
                        if (!string.IsNullOrEmpty(expr))
                        {
                            if (expr[0] == ')')
                            {
                                if (expr.Length > 1)
                                {
                                    expr = expr.Substring(1);
                                }
                                else
                                {
                                    expr = "";
                                }
                                return expar;
                            }
                        }
                    }
                }
            }
            throw new BadSyntaxException(expr);
        }
    }
}
