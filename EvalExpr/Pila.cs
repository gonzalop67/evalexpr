﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvalExpr
{
    class Pila<T>
    {
        class Nodo
        {
            public T info;
            public Nodo sig;
        }

        private Nodo raiz;

        public Pila()
        {
            raiz = null;
        }

        public void push(T x)
        {
            Nodo nuevo;
            nuevo = new Nodo();
            nuevo.info = x;
            nuevo.sig = raiz;
            raiz = nuevo;
        }

        public T pop()
        {
            T info = raiz.info;
            raiz = raiz.sig;
            return info;
        }

        public T top()
        {
            T info = raiz.info;
            return info;
        }

        public bool empty()
        {
            return raiz == null;
        }
    }
}
