﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvalExpr
{
    public class Constant : ExpressionBase, IComparable<Constant>, IEquatable<Constant>
    {
        private double _value = double.NaN;
        private string _name = null;

        public Constant()
            : base()
        {
        }
        public override double Value
        {
            get
            {
                return _value;
            }
            set
            {
                _value = value;
            }
        }
        public override string Parse(string expr)
        {
            string scon = base.Parse(expr);
            if (!string.IsNullOrEmpty(scon))
            {
                if ((char.ToLower(scon[0]) >= 'a') && (char.ToLower(scon[0]) <= 'w'))
                {
                    _name = scon.Substring(0, 1).ToLower();
                    if (scon.Length > 1)
                    {
                        return RConst(scon.Substring(1));
                    }
                    if (!ExpressionBase._constants.Contains(this))
                    {
                        ExpressionBase._constants.Add(this);
                    }
                    return "";
                }
            }
            throw new NotRecognizedException();
        }
        public int CompareTo(Constant other)
        {
            return string.Compare(_name, other._name, true);
        }
        public bool Equals(Constant other)
        {
            return string.Compare(_name, other._name, true) == 0;
        }
        public override string ToString()
        {
            return _name;
        }
        private string RConst(string expr)
        {
            if (char.IsLetterOrDigit(expr, 0))
            {
                _name += expr.Substring(0, 1).ToLower();
                if (expr.Length > 1)
                {
                    return RConst(expr.Substring(1));
                }
                expr = "";
            }
            if (!ExpressionBase._constants.Contains(this))
            {
                ExpressionBase._constants.Add(this);
            }
            return expr;
        }
    }
}
